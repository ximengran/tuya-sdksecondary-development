# 目录说明

-- LastVersion（例如 3.20.0）
  -- assets (业务包必选文件)
  -- res (业务包必选文件)
  -- sdk (Home SDK)
    -- dependencies.txt (依赖文件)
  -- ... (其他业务包或垂直类SDK)
    -- dependencies.txt (依赖文件)

# 注意事项

将 Home SDK 和其他业务包或其他 SDK 依赖拷贝至 moudle 的 build.gradle 即可完成依赖配置
其他配置可参考
Home 及垂直类 SDK 文档: https://tuyainc.github.io/tuyasmart_home_android_sdk_doc/zh-hans/
业务包文档: https://tuyainc.github.io/tuyasmart_bizbundle_android_doc/zh-hans/
